# InTouch
## Web messenger app from team №11
###Design
See [project layout](https://www.figma.com/file/Msf19iUwW2SoKI2him3Ti5/InTouch?node-id=0%3A1)
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
