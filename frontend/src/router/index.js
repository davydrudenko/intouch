import { createRouter, createWebHistory } from 'vue-router'
import Home from "../views/Home";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/auth',
    name: 'Authorization',
    component: () => import('../views/Auth')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
